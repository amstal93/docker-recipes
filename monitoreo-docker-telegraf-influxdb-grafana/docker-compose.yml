version: "3.3"

services:
  traefik:
    container_name: traefik
    image: "traefik:v2.2"
    command:
      - --entrypoints.web.address=:80
      - --entrypoints.websecure.address=:443
      - --providers.docker
      - --api
      - --api.dashboard=true
      - --log.level=DEBUG
      - --metrics=true
      - --metrics.influxdb=true
      - --metrics.influxdb.address=http://influxdb:8086
      - --metrics.influxdb.protocol=http
      - --metrics.influxdb.database=traefik
      - --metrics.influxdb.username=traefik
      - --metrics.influxdb.password=traefik
      - --accesslog=true
      - --accesslog.filepath=.logs
      - --accesslog.format=json
      - --accesslog.filters.statuscodes=200,300-302
      - --accesslog.filters.retryattempts
      - --accesslog.filters.minduration=10ms
      - --certificatesresolvers.leresolver.acme.httpchallenge=true
      - --certificatesresolvers.leresolver.acme.email=<your-email>
      - --certificatesresolvers.leresolver.acme.storage=.acme/acme.json
      - --certificatesresolvers.leresolver.acme.httpchallenge.entrypoint=web
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - ".logs:/var/log"
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
      - ".acme/acme.json:/acme.json"
    labels:
      # global redirect to https
      - "traefik.http.routers.http-catchall.rule=hostregexp(`{host:.+}`)"
      - "traefik.http.routers.http-catchall.entrypoints=web"
      - "traefik.http.routers.http-catchall.middlewares=redirect-to-https"

      # trying to make work dashboard
      - "traefik.enable=true"
      - "traefik.http.routers.traefik.rule=Host(`your-traefik-dashboard-url`)"
      - "traefik.http.routers.traefik.service=api@internal"
      - "traefik.http.routers.traefik.tls.certresolver=leresolver"
      - "traefik.http.routers.traefik.entrypoints=websecure"
      - "traefik.http.routers.traefik.middlewares=authtraefik"
      - "traefik.http.middlewares.authtraefik.basicauth.users=<your-http-password>

      # middleware redirect
      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https"

    networks:
      - "traefik-public"
      - "monitoring"

  influxdb:
    container_name: influxdb
    image: influxdb:latest
    volumes:
      - ./influxdb:/var/lib/influxdb
    labels:
      - "traefik.http.routers.influxdb.rule=Host(`influxdb`)"
      - "traefik.enable=true"
      - "traefik.http.routers.influxdb.tls.certresolver=leresolver"
      - "traefik.http.routers.influxdb.entrypoints=websecure"
      - "traefik.http.routers.influxdb.service=influxdb"
      - "traefik.http.services.influxdb.loadbalancer.server.port=8086"
    networks:
      - "monitoring"

  grafana:
    container_name: grafana
    image: grafana/grafana
    labels:
      - "traefik.http.routers.grafana.rule=Host(`grafana-url`)"
      - "traefik.enable=true"
      - "traefik.http.routers.grafana.tls.certresolver=leresolver"
      - "traefik.http.routers.grafana.entrypoints=websecure"
      - "traefik.http.routers.grafana.service=grafana"
      - "traefik.http.services.grafana.loadbalancer.server.port=3000"
    volumes:
      - "grafana:/var/lib/grafana"
    networks:
      - "monitoring"

  telegraf:
    container_name: telegraf
    image: telegraf:latest
    volumes:
      - "./telegraf.conf:/etc/telegraf/telegraf.conf:ro"
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - "monitoring"



volumes:
  influxdb:
  grafana:

networks:
  monitoring:

  traefik-public:
   external: true
